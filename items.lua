local S = minetest.get_translator("achievements_lib")

minetest.register_craftitem("achievements_lib:achievements_menu", {
    inventory_image = "achievements_lib_item.png",
    description = S("Achievments"),
    on_use = function(itemstack, player, pointed_thing)
        if player then
            local p_name = player:get_player_name()
            achvmt_lib.show_achievements_fs(p_name)
        end
    end
})
