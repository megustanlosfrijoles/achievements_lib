local S = minetest.get_translator("achievements_lib")

----------------------------------------------
-----------      Init tables     -------------
----------------------------------------------

achvmt_lib.achievements = {}
achvmt_lib.mods = {} -- table of achievements, indexed by mod
achvmt_lib.player_achievements = {}
achvmt_lib.player_ach_backlog = {}

local storage = minetest.get_mod_storage("achievements_lib")

----------------------------------------------
-----------       Settings       -------------
----------------------------------------------

achvmt_lib.hud_popup_time = 7

----------------------------------------------
-----------Predeclare local funcs-------------
----------------------------------------------

-- storage
local function store_player_achievements(p_name) end
local function recall_player_achievements(p_name) end

-- backlog to show hud popups when player logs on if they have not seen the
-- popups yet (awarded while offline; maybe team challenges were met, or they
-- won a tournament or something)
local function store_achievement_backlog(p_name) end
local function recall_achievement_backlog(p_name) end

-- often-used checks
local function try_to_load_player(p_name) end

----------------------------------------------
----------------    API     ------------------
----------------------------------------------

function achvmt_lib.register_achievement(name, def)

    -- get the mod in the acheivement registration
    local splitter_pos = string.find(name, ":")

    local get_mod = true

    local mod

    if not (splitter_pos) then
        minetest.debug("[ERROR] [Achievements_lib] an achievement (" .. name ..
                           ") was registered without a ':' splitter. The Achievement will not be registered.")
        get_mod = false
    elseif splitter_pos == 1 then
        minetest.debug("[ERROR] [Achievements_lib] an achievement (" .. name ..
                           ") was registered without a mod. The Achievement will not be registered.")
        get_mod = false
    end
    if get_mod == false then return end
    mod = string.sub(name, 1, splitter_pos - 1)
    local modpath = minetest.get_modpath(mod)
    if not modpath then
        minetest.debug("[ERROR] [Achievements_lib] an achievement (" .. name ..
                           ") was registered with an invalid mod.")
        return
    end

    def.title = def.title or S("Achievement!") -- Please, define your values, or this is your fate.
    def.description = def.description or "" -- Description is completely optional.

    def.mod_title = def.mod_title or def.mod or ""
    def.mod_icon = def.mod_icon or "transparent.png"
    def.image = def.image or "achievements_lib_item.png"
    def.image_size = def.image_size or 16
    def.tier = def.tier or "Bronze"
    def.hidden = def.hidden or def.secret or false
    def.on_award = def.on_award or function(p_name) return end
    def.on_unaward = def.on_unaward or function(p_name) return end
    def.name = name -- for easy cross-reference

    if get_mod then
        def.mod = mod
        achvmt_lib.mods[mod] = achvmt_lib.mods[mod] or {}
        table.insert(achvmt_lib.mods[mod], def)
        achvmt_lib.mods[mod].mod_icon = def.mod_icon
        achvmt_lib.mods[mod].mod_title = def.mod_title
        achvmt_lib.mods[mod].num_achievements =
            (achvmt_lib.mods[mod].num_achievements or 0) + 1
    else
        def.mod = ""
    end

    achvmt_lib.num_achievements = (achvmt_lib.num_achievements or 0) + 1

    achvmt_lib.achievements[name] = def

end

function achvmt_lib.get_achievements(filter)
    if filter == nil then return achvmt_lib.achievements end
    local filter = filter or {}
    local ret = {}
    for name, ach_def in pairs(achvmt_lib.achievements) do
        local include = true
        if filter.mod and ach_def.mod and ach_def.mod ~= filter.mod then
            include = false
        end
        if filter.tier and ach_def.tier and ach_def.tier ~= filter.tier then
            include = false
        end
        if include then ret[name] = ach_def end
    end
    
    return ret
end

function achvmt_lib.has_achievement(p_name, achievement_name)
    if not (try_to_load_player(p_name)) then return false end
    for i, ach_name in ipairs(achvmt_lib.player_achievements[p_name]) do
        if ach_name == achievement_name then return i end
    end

    return false
end

function achvmt_lib.get_player_achievements(p_name, return_defs, filter)
    local player_exists, is_offline = try_to_load_player(p_name)

    if not (player_exists) then return end

    filter = filter or {}
    return_defs = return_defs or false

    if filter == {} and return_defs == false then

        return achvmt_lib.player_achievements[p_name]
    else
        local ret = {}
        for i, ach_name in pairs(achvmt_lib.player_achievements[p_name]) do
            local ach_def = achvmt_lib.achievements[ach_name]
            if ach_def then
                local include = true
                if filter.mod and ach_def.mod and ach_def.mod ~= filter.mod then
                    include = false
                end
                if filter.tier and ach_def.tier and ach_def.tier ~= filter.tier then
                    include = false
                end
                if include then
                    if return_defs then
                        ret[ach_name] = ach_def
                    else
                        table.insert(ret, ach_name)
                    end
                end
            end
        end

        return ret
    end
end

function achvmt_lib.award(ach_name, p_name)
    -- checks
    -- can't award to a player that does not exist
    local player_exists, is_offline = try_to_load_player(p_name)
    if not (player_exists) then return false end

    -- can't award an achievement that does not exist
    local ach_def = achvmt_lib.achievements[ach_name]
    if not ach_def then return false end

    -- can't award an achievement the player already has. 
    if achvmt_lib.has_achievement(p_name, ach_name) then return false end

    -- give achievement and save it
    table.insert(achvmt_lib.player_achievements[p_name], 1, ach_name)
    store_player_achievements(p_name)
    -- store popup backlog if player is offline, or show popup now if they are
    -- online
    if is_offline then
        table.insert(achvmt_lib.player_ach_backlog[p_name], ach_name)
        store_achievement_backlog(p_name)
    else
        achvmt_lib.show_hud_achievement(p_name, ach_def,
                                        achvmt_lib.hud_popup_time)
    end

    ach_def.on_award(p_name)

    return true
end

function achvmt_lib.unaward(ach_name, p_name)
    -- checks
    -- can't unaward to a player that does not exist
    local player_exists, is_offline = try_to_load_player(p_name)
    if not (player_exists) then return false end

    -- try to find achievement
    local ach = achvmt_lib.has_achievement(p_name, ach_name)
    -- remove achievement and save it
    if ach then
        -- found it! remove it. save it. return success.
        if achvmt_lib.achievements[ach_name] then
            achvmt_lib.achievements[ach_name].on_unaward(p_name)
        end
        table.remove(achvmt_lib.player_achievements[p_name], ach)
        store_player_achievements(p_name)
        
        return true
    end
    -- didn't find it; the player did not have that achievement. 

    return false
end

function achvmt_lib.unaward_all(p_name)
    -- checks
    -- can't unaward to a player that does not exist
    if not (try_to_load_player(p_name)) then return false end
    -- run callbacks
    for i, ach_name in ipairs(achvmt_lib.player_achievements[p_name]) do
        if achvmt_lib.achievements[ach_name] then
            achvmt_lib.achievements[ach_name].on_unaward(p_name)
        end
    end
    -- clear player's achievements
    achvmt_lib.player_achievements[p_name] = {}
    -- save
    store_player_achievements(p_name)

    return true
end

----------------------------------------------
--------------- Joinplayer  ------------------
----------------------------------------------

minetest.register_on_joinplayer(function(player, last_login)
    local p_name = player:get_player_name()

    if last_login == nil then
        -- new player, init tables and storage
        achvmt_lib.player_achievements[p_name] = {}
        store_player_achievements(p_name) -- update storage with the new key
        achvmt_lib.player_ach_backlog[p_name] = {}
    else
        recall_player_achievements(p_name)
        -- Show achievement backlog (TODO)
        recall_achievement_backlog(p_name)
        local backlog = achvmt_lib.player_ach_backlog[p_name]
        -- TODO: show backlog in for loop. 
        -- delete backlog, save.
        achvmt_lib.player_ach_backlog[p_name] = {}
    end

    store_achievement_backlog(p_name)
end)

----------------------------------------------
----------- Local funcs defined --------------
----------------------------------------------

function store_player_achievements(p_name)
    local ser = minetest.serialize(achvmt_lib.player_achievements[p_name])
    storage:set_string("achvmts_" .. p_name, ser)
end

function recall_player_achievements(p_name)
    local ser = storage:get_string("achvmts_" .. p_name)
    achvmt_lib.player_achievements[p_name] = minetest.deserialize(ser) or {}
end

function store_achievement_backlog(p_name)
    local ser = minetest.serialize(achvmt_lib.player_ach_backlog[p_name])
    storage:set_string("backlog_" .. p_name, ser)
end

function recall_achievement_backlog(p_name)
    local ser = storage:get_string("backlog_" .. p_name)
    achvmt_lib.player_ach_backlog[p_name] = minetest.deserialize(ser) or {}
end

function try_to_load_player(p_name)
    local player_exists = true
    local is_offline = false
    if not achvmt_lib.player_achievements[p_name] then
        recall_player_achievements(p_name)
        is_offline = true
    end
    if not achvmt_lib.player_achievements[p_name] then
        player_exists = false
        is_offline = false
    end

    return player_exists, is_offline
end
