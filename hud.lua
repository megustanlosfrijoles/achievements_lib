local S = minetest.get_translator("achievements_lib")

-- plays a sound when the popup pops up
function achvmt_lib.play_ach_sound(p_name)
    local ss = "achievements_lib_acheievement"
    local sp = {to_player = p_name}
    minetest.sound_play(ss, sp)
end

-- GUI (HUD POPUP)
function achvmt_lib.show_hud_achievement(p_name, ach_def, lasts_for)

    local player = minetest.get_player_by_name(p_name)
    if not player then return end

    achvmt_lib.play_ach_sound(p_name)

    local idx1 = player:hud_add({
        position = {x = 1, y = 1},
        alignment = {x = -1, y = -1},
        scale = {x = 5, y = 5},
        offset = {x = 0, y = 0},
        hud_elem_type = "image",
        name = "leaderboard_bg",
        text = "achievements_lib_hud_bg.png"
    })
    -- ach image
    local imgscale = (64 / ach_def.image_size)
    local idx2 = player:hud_add({
        hud_elem_type = "image",
        text = ach_def.image,
        scale = {x = imgscale, y = imgscale},
        position = {x = 1, y = 1},
        alignment = {x = 1, y = 1},
        offset = {x = -77 * 5, y = -15 * 5}
    })
    -- title
    local idx3 = player:hud_add({
        hud_elem_type = "text",
        text = ach_def.title,
        number = 0x472D3C,
        scale = {x = 61 * 5, y = 6 * 5},
        position = {x = 1, y = 1},
        alignment = {x = 1, y = 1},
        offset = {x = -61 * 5, y = -15 * 5},
        style = 1,
        size = {x = 1.2}
    })
    -- ach description
    local idx4 = player:hud_add({
        hud_elem_type = "text",
        text = ach_def.description,
        number = 0x472D3C,
        scale = {x = 61 * 5, y = 6 * 5},
        position = {x = 1, y = 1},
        alignment = {x = 1, y = 1},
        offset = {x = -61 * 5, y = -6.5 * 5},
        style = 2,
        size = {x = 1}
    })

    minetest.after(achvmt_lib.hud_popup_time, function()
        local player = minetest.get_player_by_name(p_name)
        if not player then return end

        player:hud_remove(idx1)
        player:hud_remove(idx2)
        player:hud_remove(idx3)
        player:hud_remove(idx4)
    end)

    minetest.chat_send_player(p_name, 
        minetest.colorize("#a0938e", 
            "[" .. S("Achievement Unlocked") .. "] " .. ach_def.title))

end
