local S = minetest.get_translator("achievements_lib")

local player_scroll_indexes = {}
local players_viewing = {} -- whose acheivements each player is viewing
local player_tabs = {}
local view_settings = {}
local first_visible_tab = {} -- first visible tab for each player

----------------------------------------------
-----------Predeclare local funcs-------------
----------------------------------------------

-- This Function MIT by Rubenwardy
--- Creates a scrollbaroptions for a scroll_container
--
-- @param visible_l the length of the scroll_container and scrollbar
-- @param total_l length of the scrollable area
-- @param scroll_factor as passed to scroll_container
local function make_scrollbaroptions_for_scroll_container(visible_l, total_l,
                                                          scroll_factor, arrows)
    return
end

-- Makes and returns the achivement list that goes in the scroll container. Also
-- returns the formspec-length of the list for the scrollbar options. @p_name is
-- the player who has the achievments, @to_p_name is the player to whom the
-- achievements are shown, defaults to p_name. @filter is a filter table as
-- detailed in DOCS.md, and @view is either "U", "A", or "AU"; a filter for
-- limiting the shown achievements to: U= unachieved, A=achieved, AU= show all
local function get_achievement_list_fs(p_name, to_p_name, filter, view) return end

-- returns table of available mod tabnames
local function get_available_tabs() return end

-- returns the bottom tabs formspec
local function get_tabs() return end
----------------------------------------------
----------------    API     ------------------
----------------------------------------------

-- GUI (FORMSPEC)
function achvmt_lib.show_achievements_fs(p_name, to_p_name)
    to_p_name = to_p_name or p_name
    players_viewing[to_p_name] = p_name -- keep track of which achievements fs to_p_name is viewing
    player_tabs[to_p_name] = player_tabs[to_p_name] or "ALL" -- the tab name for each player
    view_settings[to_p_name] = view_settings[to_p_name] or "A" -- "A": Achieved, "U": Unachieved, "AU": both
    local visible_l = 5.875

    local tot_all_ach = achvmt_lib.num_achievements or 0

    -- filter by mod if in a mod tab
    local filter = {}
    if player_tabs[to_p_name] ~= "ALL" then
        filter.mod = player_tabs[to_p_name]
        tot_all_ach = achvmt_lib.mods[player_tabs[to_p_name]].num_achievements
    end
    -- more counters
    local tot_achieved_ach = #achvmt_lib.get_player_achievements(p_name, false,
                                                                 filter)
    local tot_unachieved_ach = tot_all_ach - tot_achieved_ach

    local use_reward_bg = tot_achieved_ach == tot_all_ach

    local ach_list, total_l, tot_p_ach =
        get_achievement_list_fs(p_name, to_p_name, filter,
                                view_settings[to_p_name])

    player_scroll_indexes[to_p_name] = player_scroll_indexes[to_p_name] or 0

    local fs = "formspec_version[6]"
    fs = fs .. "size[15,10]"
    fs = fs .. "padding[.01,.01]"
    fs = fs .. "no_prepend[]"
    fs = fs .. "real_coordinates[true]"
    fs = fs .. "bgcolor[;neither;]"
    if use_reward_bg then
        fs = fs .. "background[0,0;15,10;achievements_lib_fs_bg_ach.png]"
    else
        fs = fs .. "background[0,0;15,10;achievements_lib_fs_bg.png]"
    end
    fs = fs ..
             "style[TITLE;border=false;textcolor=#302c2e;font=bold;font_size=*1.5]"
    fs =
        fs .. "button[2,1;11,1;TITLE;" .. S("Achievements") .. " (" .. p_name ..
            ")" .. "]"
    fs = fs .. "style[P_NAME;border=false;font=bold;font_size=*.9]"
    fs = fs .. "style[COVER;border=false]"
    fs = fs .. "button[2,1;8,1;COVER;]"

    fs = fs .. "style_type[image_button;border=false]"

    -- get images for filter icons ... the seleced one gets a green highlight
    local both = "achievements_lib_both.png"
    local has = "achievements_lib_has.png"
    local hasnt = "achievements_lib_hasnt.png"
    if view_settings[to_p_name] == "A" then
        has = has .. "^achievements_lib_filter_selected.png"
    elseif view_settings[to_p_name] == "U" then
        hasnt = hasnt .. "^achievements_lib_filter_selected.png"
    elseif view_settings[to_p_name] == "AU" then
        both = both .. "^achievements_lib_filter_selected.png"
    end

    fs = fs .. "image_button[" .. 14 / 16 .. "," .. 40 / 16 .. ";.75,.75;" ..
             both .. ";BOTH;]"
    fs = fs .. "tooltip[" .. 14 / 16 .. "," .. 40 / 16 .. ";.75,.75;" ..
             S("Show All") .. " (" .. tot_all_ach .. "/" .. tot_all_ach .. ")" ..
             ";#7a444a;#f4cca1]"

    fs = fs .. "image_button[" .. 14 / 16 .. "," .. 60 / 16 .. ";.75,.75;" ..
             has .. ";HAS;]"
    fs = fs .. "tooltip[" .. 14 / 16 .. "," .. 60 / 16 .. ";.75,.75;" ..
             S("Filter by Achieved") .. " (" .. tot_achieved_ach .. "/" ..
             tot_all_ach .. ")" .. ";#7a444a;#f4cca1]"

    fs = fs .. "image_button[" .. 14 / 16 .. "," .. 80 / 16 .. ";.75,.75;" ..
             hasnt .. ";HASNT;]"
    fs = fs .. "tooltip[" .. 14 / 16 .. "," .. 80 / 16 .. ";.75,.75;" ..
             S("Filter by Unachieved") .. " (" .. tot_unachieved_ach .. "/" ..
             tot_all_ach .. ")" .. ";#7a444a;#f4cca1]"

    fs = fs .. "scroll_container[2.125,2.625;10.8125," .. visible_l ..
             ";ACH_SCROLL;vertical;.1]"
    fs = fs .. ach_list
    fs = fs .. "scroll_container_end[]"
    if total_l >= visible_l then
        fs = fs ..
                 make_scrollbaroptions_for_scroll_container(visible_l, total_l,
                                                            .1, "hide")
        fs = fs .. "scrollbar[12.8125,2.625;.125," .. visible_l ..
                 ";vertical;ACH_SCROLL;" .. player_scroll_indexes[to_p_name] ..
                 "]"
    end
    fs = fs .. get_tabs(p_name, to_p_name)

    minetest.show_formspec(to_p_name, "achvmt_lib_disp", fs)
end

----------------------------------------------
-------------  Functions defined -------------
----------------------------------------------

-- This Function MIT by Rubenwardy
--- Creates a scrollbaroptions for a scroll_container
--
-- @param visible_l the length of the scroll_container and scrollbar
-- @param total_l length of the scrollable area
-- @param scroll_factor as passed to scroll_container
function make_scrollbaroptions_for_scroll_container(visible_l, total_l,
                                                    scroll_factor, arrows)
    assert(total_l >= visible_l)
    arrows = arrows or "default"
    local thumb_size = (visible_l / total_l) * (total_l - visible_l)
    local max = total_l - visible_l

    return ("scrollbaroptions[min=0;max=%f;thumbsize=%f;arrows=%s]"):format(
               max / scroll_factor, thumb_size / scroll_factor, arrows)
end

function get_achievement_list_fs(p_name, to_p_name, filter, view) 
    -- filter is for mod, view is A,U,or AU and filters per-player by whether
    -- they have the achievement or not
    local list_height_multiplier = 1.5

    local p_achievements_ordered = achvmt_lib.get_player_achievements(p_name, false, filter) or {}
    local p_achievements_unordered = achvmt_lib.get_player_achievements(p_name, true, filter) or {}
    local all_achievements = achvmt_lib.get_achievements(filter)

    local fs = ""
    local total_h = 0

    local color_flip = false
    local count = 0
    local bgimg_1 = "achievements_lib_fs_list_bg1.png"
    local bgimg_2 = "achievements_lib_fs_list_bg2.png"

    -- order player's achievements first, then unachieved ones. Totally secret,
    -- locked achievements go last.

    local ach_list = {}
    local secret_ach_list = {}
    local tot_p_ach = 0

    for i, ach_name in ipairs(p_achievements_ordered) do
        if string.find(view, "A") then
            table.insert(ach_list, achvmt_lib.achievements[ach_name])
            tot_p_ach = tot_p_ach + 1
        end
    end
    for ach_name, ach in pairs(all_achievements) do
        if string.find(view, "U") then
            -- tests for player having an achievement, use this instead of the
            -- built-in function to reduce calculation
            if not (p_achievements_unordered[ach_name]) then
                -- store secret achievements to add in last
                if ach.secret then
                    table.insert(secret_ach_list, ach)
                else
                    table.insert(ach_list, ach)
                end
                tot_p_ach = tot_p_ach + 1
            end
        end
    end

    -- add in any secret achievements last
    for i, ach in ipairs(secret_ach_list) do table.insert(ach_list, ach) end

    -- get unordered list of player achievements for to_p_name to test if they
    -- have an achivement, to know whether to hide stuff.
    local to_p_achvmts = achvmt_lib.get_player_achievements(to_p_name, true,
                                                            filter) or {}

    -- build the ach list fs from the ach_list
    for i, ach in ipairs(ach_list) do
        local ach_name = ach.name
        local p_ach = p_achievements_unordered[ach_name]
        local to_p_ach = to_p_achvmts[ach_name]

        -- if not(p_ach) then
        --     bgimg_1 = "achievements_lib_fs_unachlist_bg1.png"
        --     bgimg_2 = "achievements_lib_fs_unachlist_bg2.png"
        -- end
        -- alternate bg colors in list
        local bg_img = bgimg_1
        if color_flip then bg_img = bgimg_2 end
        color_flip = not (color_flip)

        local y_pos = (count) * list_height_multiplier
        local tier_image = "achievements_lib_" .. (ach.tier or "Bronze") ..
                               ".png"
        local ach_title = ach.title
        local tier_hovertext = (ach.tier or "Bronze") .. " " .. S("Tier")
        local mod_icon = ach.mod_icon or "transparent.png"
        local mod_text = ach.mod_title or ach.mod or ""
        local ach_hidden_text = ach.hint or "???" .. " " -- need space so minetest doesnt cut off the italics text
        local ach_image = ach.image
        local ach_description = ach.description .. " "
        local text_color = "#472d3c"

        if ach.secret and not to_p_ach then
            ach_title = "???"
            ach_image = "achievements_lib_secret.png"
            ach_hidden_text = "???" .. " "
            tier_image = "achievements_lib_secret_tier.png"
            tier_hovertext = "???"
        end

        total_h = total_h + list_height_multiplier

        fs = fs .. "image[0," .. y_pos .. ";10.75," .. list_height_multiplier ..
                 ";" .. bg_img .. "]"

        -- From mod
        fs = fs .. "image[" .. (0.2) .. "," .. (y_pos + .1) .. ";" ..
                 (list_height_multiplier - 0.2) / 2 .. "," ..
                 (list_height_multiplier - 0.2) / 2 .. ";" .. mod_icon .. "]"
        fs = fs .. "tooltip[" .. (0.2) .. "," .. (y_pos + .1) .. ";" ..
                 (list_height_multiplier - 0.2) / 2 .. "," ..
                 (list_height_multiplier - 0.2) / 2 .. ";" .. mod_text ..
                 ";#7a444a;#f4cca1]"

        -- tier
        fs = fs .. "image[" .. (0.2) .. "," ..
                 (y_pos + list_height_multiplier / 2) .. ";" ..
                 (list_height_multiplier - 0.2) / 2 .. "," ..
                 (list_height_multiplier - 0.2) / 2 .. ";" .. tier_image .. "]"
        fs = fs .. "tooltip[" .. (0.2) .. "," ..
                 (y_pos + list_height_multiplier / 2) .. ";" ..
                 (list_height_multiplier - 0.2) / 2 .. "," ..
                 (list_height_multiplier - 0.2) / 2 .. ";" .. tier_hovertext ..
                 ";#7a444a;#f4cca1]"

        -- ach image
        fs = fs .. "image[" .. (0.2 + list_height_multiplier / 2) .. "," ..
                 (y_pos + .1) .. ";" .. (list_height_multiplier - 0.2) .. "," ..
                 (list_height_multiplier - .2) .. ";" .. ach_image .. "]"

        -- Title
        fs = fs .. "style_type[label;border=false;textcolor=" .. text_color ..
                 ";font=bold;font_size=*1.2]"
        fs = fs .. "label[" .. (0.2 + 1.5 * list_height_multiplier) .. "," ..
                 (y_pos + .3) .. ";" .. ach_title .. "]"
        -- info icons:

        -- description info (hidden if ach.hidden is true, and to_p_name has not
        -- unlocked it.)
        fs = fs .. "style_type[label;border=false;textcolor=" .. text_color ..
                 ";font=italic;font_size=*0.9]"

        if ach.hidden and not (to_p_ach) then
            fs =
                fs .. "label[" .. (0.2 + 1.5 * list_height_multiplier) .. "," ..
                    (y_pos + .1 + list_height_multiplier / 2) .. ";" ..
                    ach_hidden_text .. "]"
        else
            fs =
                fs .. "label[" .. (0.2 + 1.5 * list_height_multiplier) .. "," ..
                    (y_pos + .1 + list_height_multiplier / 2) .. ";" ..
                    ach_description .. "]"
        end

        -- grey out if unachieved
        if not p_ach then
            fs = fs .. "image[0," .. (y_pos - .02) .. ";10.75," ..
                     (list_height_multiplier + 0.02) ..
                     ";achievements_lib_grey_out.png]"
        end

        count = count + 1
    end
    if count == 0 then
        fs = fs .. "image[0,0;10.75," .. list_height_multiplier ..
                 ";achievements_lib_fs_unachlist_bg2.png]"
        fs = fs ..
                 "style[INFO;border=false;textcolor=#dff6f5;font=italic;font_size=*1]"
        fs = fs .. "button[0,0;10.75," .. list_height_multiplier .. ";INFO;" ..
                 S("No achievements ... yet") .. "]"
        total_h = list_height_multiplier
    end

    return fs, total_h, tot_p_ach

end

function get_available_tabs()
    local available_tabs = {}
    table.insert(available_tabs, "ALL")
    for modname, achs in pairs(achvmt_lib.mods) do
        table.insert(available_tabs, modname)
    end
    
    return available_tabs
end

function get_tabs(p_name, to_p_name)
    local available_tabs = {}
    local max_visible_tabs = 9
    local tab_size = 15.5 / 16
    local start_x = (43.5) / 16
    local fs = ""
    local available_tabs = get_available_tabs()
    first_visible_tab[to_p_name] = first_visible_tab[to_p_name] or 1

    local num_max_all_ach = achvmt_lib.num_achievements or 0

    local visible_tabs = {}

    for i = first_visible_tab[to_p_name], first_visible_tab[to_p_name] +
        max_visible_tabs do
        if available_tabs[i] then
            table.insert(visible_tabs, available_tabs[i])
        end
    end

    for i, tabname in ipairs(visible_tabs) do -- tabname is modname or "ALL"
        local x, y = start_x + (i - 1) * tab_size, 137 / 16
        local w, h = tab_size, tab_size
        local texture
        local tooltip
        local num_tab_ach = num_max_all_ach
        if tabname == "ALL" then
            texture = "achievements_lib_item.png"
            tooltip = S("All Achievements")
        else
            num_tab_ach = achvmt_lib.mods[tabname].num_achievements
            texture = achvmt_lib.mods[tabname].mod_icon
            tooltip = S("Achievements from") .. " " ..
                          achvmt_lib.mods[tabname].mod_title
        end
        tooltip = tooltip .. " (" .. num_tab_ach .. "/" .. num_max_all_ach ..
                      ")"
        fs = fs .. "style_type[image_button;border=false]"
        if player_tabs[to_p_name] == tabname then
            fs = fs .. "image[" .. x .. "," .. y .. ";" .. w .. "," .. h ..
                     ";achievements_lib_tab_bg_selected.png]"
        else
            fs = fs .. "image[" .. x .. "," .. y .. ";" .. w .. "," .. h ..
                     ";achievements_lib_tab_bg.png]"
        end
        fs =
            fs .. "image_button[" .. x + .1 .. "," .. y + .1 .. ";" .. w - .2 ..
                "," .. h - .2 .. ";" .. texture .. ";" .. tabname .. ";]"
        fs = fs .. "tooltip[" .. x .. "," .. y .. ";" .. w .. "," .. h .. ";" ..
                 tooltip .. ";#7a444a;#f4cca1]"
    end
    -- show arrows
    if (#available_tabs > max_visible_tabs) and
        (first_visible_tab[to_p_name] ~= 1) then
        fs = fs .. "image_button[" .. 32 / 16 .. "," .. 138 / 16 .. ";" .. 8 /
                 16 .. "," .. 14 / 16 ..
                 ";achievements_lib_arrow_left.png;TAB_LEFT;]"
    end
    if (#available_tabs > max_visible_tabs) and
        (first_visible_tab[to_p_name] ~= #available_tabs - max_visible_tabs) then
        fs = fs .. "image_button[" .. 201 / 16 .. "," .. 138 / 16 .. ";" .. 8 /
                 16 .. "," .. 14 / 16 ..
                 ";achievements_lib_arrow_right.png;TAB_RIGHT;]"
    end

    return fs
end

----------------------------------------------
-------------      Callback      -------------
----------------------------------------------

minetest.register_on_player_receive_fields(
    function(player, formname, fields)
        local p_name = player:get_player_name()
        if formname ~= "achvmt_lib_disp" then return end
        local available_tabs = get_available_tabs()
        if fields.HAS then
            view_settings[p_name] = "A"
            achvmt_lib.show_achievements_fs(players_viewing[p_name], p_name)
        end
        if fields.HASNT then
            view_settings[p_name] = "U"
            achvmt_lib.show_achievements_fs(players_viewing[p_name], p_name)
        end
        if fields.BOTH then
            view_settings[p_name] = "AU"
            achvmt_lib.show_achievements_fs(players_viewing[p_name], p_name)
        end
        -- mod tabs
        for _, tabname in ipairs(available_tabs) do
            if fields[tabname] then
                player_tabs[p_name] = tabname
                achvmt_lib.show_achievements_fs(players_viewing[p_name], p_name)
            end
        end
        -- move mod tab arrows; buttons wont be shown if they are not supposed to
        -- be, so we dont have to check.
        if fields.TAB_LEFT then
            first_visible_tab[p_name] = first_visible_tab[p_name] - 1
            achvmt_lib.show_achievements_fs(players_viewing[p_name], p_name)
        end
        if fields.TAB_RIGHT then
            first_visible_tab[p_name] = first_visible_tab[p_name] + 1
            achvmt_lib.show_achievements_fs(players_viewing[p_name], p_name)
        end

    end)
