local version = "12.27.22"

achvmt_lib = {}

if not minetest.get_modpath("lib_chatcmdbuilder") then
    dofile(minetest.get_modpath("achievements_lib") .. "/chatcmdbuilder.lua")
end

dofile(minetest.get_modpath("achievements_lib") .. "/api.lua")
dofile(minetest.get_modpath("achievements_lib") .. "/gui.lua")
dofile(minetest.get_modpath("achievements_lib") .. "/hud.lua")
dofile(minetest.get_modpath("achievements_lib") .. "/items.lua")
dofile(minetest.get_modpath("achievements_lib") .. "/commands.lua")
-- dofile(minetest.get_modpath("achievements_lib") .. "/tests.lua")

minetest.log("action",
             "[ACHIEVEMENTS_LIB] Mod initialised, running version " .. version)
