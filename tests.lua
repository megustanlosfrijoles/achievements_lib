achvmt_lib.register_achievement("achievements_lib:achvmt_lib_tests_ach_1", {
    title = "Ultimate Bounciness",
    description = "Bounce on a trampoline 5 times.",
    mod = "achievements_lib",
    mod_icon = "logo.png",
    mod_title = "Achievements Lib",
    image = "default_tree.png",
    tier = "Bronze",
    hidden = false
})

achvmt_lib.register_achievement("achievements_lib:achvmt_lib_tests_ach_2", {
    title = "Penultimate Bounciness",
    description = "Bounce on a trampoline 20 times.",
    mod = "achievements_lib",
    mod_icon = "logo.png",
    mod_title = "Achievements Lib",
    image = "default_tree_top.png",
    tier = "Silver",
    hidden = false
})

achvmt_lib.register_achievement("achievements_lib:achvmt_lib_tests_ach_3", {
    title = "UltiPenultimate Bounciness",
    description = "Bounce on a trampoline 200 times.",
    mod = "achievements_lib",
    mod_icon = "logo.png",
    mod_title = "Achievements Lib",
    image = "default_stone.png",
    tier = "Gold",
    hidden = false
})

achvmt_lib.register_achievement("achievements_lib:achvmt_lib_tests_ach_4", {
    title = "More Bounciness",
    description = "Bounce on a trampoline 400 times.",
    mod = "achievements_lib",
    mod_icon = "logo.png",
    mod_title = "Achievements Lib",
    image = "default_cobble.png",
    tier = "Gold",
    hidden = false
})

achvmt_lib.register_achievement("achievements_lib:achvmt_lib_tests_ach_5", {
    title = "Even More Bounciness",
    description = "Bounce on a trampoline 500 times.",
    mod = "achievements_lib",
    mod_icon = "logo.png",
    mod_title = "Achievements Lib",
    image = "default_stone_block.png",
    tier = "Gold",
    hidden = false
})

achvmt_lib.register_achievement("default:stick", {
    title = "Stick",
    description = "Get a stick",
    mod_icon = "default_stone.png",
    mod_title = "Default",
    image = "default_stick.png",
    image_size = 16,
    tier = "Bronze",
    hidden = false
})

achvmt_lib.register_achievement("fireflies:firefly", {
    title = "firefly",
    description = "catch a firefly",
    mod_icon = "fireflies_bottle.png",
    mod_title = "Fireflies",
    image = "fireflies_bugnet.png",
    image_size = 16,
    tier = "Bronze",
    hidden = true
})

achvmt_lib.register_achievement("balloon_bop:bop", {
    title = "bop",
    description = "Bop",
    mod_icon = "balloon_bop_hudballoon.png",
    mod_title = "balloon_bop",
    image = "balloon_bop_hudballoon.png",
    image_size = 16,
    tier = "Bronze",
    hidden = false
})

achvmt_lib.register_achievement("flowers:waterlilly", {
    title = "Floating Point",
    description = "Get a waterlilly",
    mod_icon = "flowers_waterlily.png",
    mod_title = "flowers",
    image = "flowers_waterlily.png",
    image_size = 16,
    tier = "Bronze",
    hidden = false
})

achvmt_lib.register_achievement("bucket:bucket", {
    title = "Bucket",
    description = "Get a bucket",
    mod_icon = "bucket.png",
    mod_title = "Bucket",
    image = "bucket.png",
    image_size = 16,
    tier = "Bronze",
    secret = true
})

achvmt_lib.register_achievement("arena_lib:music", {
    title = "Music",
    description = "set music",
    mod_icon = "arenalib_customise_bgm.png",
    mod_title = "Arena Lib",
    image = "arenalib_customise_bgm.png",
    image_size = 16,
    tier = "Bronze",
    secret = false
})

achvmt_lib.register_achievement("worldedit:wand", {
    title = "Wand",
    description = "Use wand",
    mod_icon = "worldedit_wand.png",
    mod_title = "World Edit",
    image = "worldedit_wand.png",
    image_size = 16,
    tier = "Bronze",
    secret = false
})

-- show another player's achievements on rightclick
minetest.register_on_rightclickplayer(function(player, clicker)
    local p_name = player:get_player_name()
    local to_p_name = clicker:get_player_name()
    achvmt_lib.show_achievements_fs(p_name, to_p_name)
end)
