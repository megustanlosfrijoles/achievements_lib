local S = minetest.get_translator("achievements_lib")

-- show fs command
minetest.register_chatcommand("achievements", {
    description = S("See your achievements"),
    func = function(p_name, param)
        local player = minetest.get_player_by_name(p_name)
        if player then achvmt_lib.show_achievements_fs(p_name) end
    end
})

minetest.register_privilege("achvmt_lib_admin",
                            {description = S("Award Achievements")})

ChatCmdBuilder.new("achvmt_lib", function(cmd)

    -- create arena
    cmd:sub("award :player :AchievementName",
            function(name, p_name, achievement_name)
        if achvmt_lib.achievements[achievement_name] then
            achvmt_lib.award(achievement_name, p_name)
            -- return true, "[achvmt_lib] " .. S("Achievement") .. " " .. achievement_name .. " " .. S("awarded to") .. " " .. p_name .. "."
            return true, "[achvmt_lib] " ..
                       S("Achievement @1 awarded to @2", achievement_name,
                         p_name)

        else
            return false, "[achvmt_lib] " .. "[!] " ..
                       S("No achievement by the name @1", achievement_name)
        end
    end)

    -- create arena
    cmd:sub("unaward :player :AchievementName",
            function(name, p_name, achievement_name)
        if achievement_name == "all" then
            achvmt_lib.unaward_all(p_name)
            return true, "[achvmt_lib] " ..
                       S("All Achievements removed from @1", p_name)
        else
            if achvmt_lib.achievements[achievement_name] then
                achvmt_lib.unaward(achievement_name, p_name)
                return true, "[achvmt_lib] " ..
                           S("Achievement @1 removed from @2", achievement_name,
                             p_name)
            else
                return false, "[achvmt_lib] " .. "[!] " ..
                           S("No achievement by the name @1", achievement_name)
            end
        end
    end)

end, {
    description = [[
      (/help achvmt_lib)
      Use this to award or revoke achievements:
      - award <p_name> <achievement_name> -- awards an achievement to a player
      - unaward <p_name> <achievement_name> -- removes an achievement from a player
      - unaward <p_name> all -- removes all achievements from a player
      ]],
    privs = {achvmt_lib_admin = true}
})
