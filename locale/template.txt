# version 12.28.22
# author(s):
# reviewer(s):
# textdomain: achievements_lib

# api.lua
Achievement!=
You got an Achievement!=

# commands.lua
See your achievements=
Award Achievements=
Achievement @1 awarded to @2=
No achievement by the name @1=
All Achievements removed from @1=
Achievement @1 removed from @2=


# gui.lua
Achievement Unlocked=
Achievements=
Tier=
Unlock to view requirements=
No achievements ... yet=
Show All=
Filter by Achieved=
Filter by Unachieved=
Achievements from=
All Achievements=