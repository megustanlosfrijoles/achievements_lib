
Achievements_lib is a library that handles achievements. It provides a system
for awarding achievements and viewing them, as well as a HUD notification on
award.

## Data Structures

`achvmt_lib.achievements`: the table of achievements, indexed by name. Each
  element is a table with the values from achievement registration.

`achvmt_lib.player_achievements`: the table, indexed by player name, which
  contains lists of achievement names that each player has unlocked. This only
  contains players who are online. Read-only; use `achvmt_lib.award` and
  `achvmt_lib.unaward` to modify it. This is an ordered list (you can use
  ipairs), ordered in the reverse chronological order that the player was
  awarded each achievement. (so more recent achievements are at the top)

## Registering an achievement

`achvmt_lib.register_achievement(technical_name, achievement_def)`: registers an
achievement.

  `technical_name`: such as `mymod:ultimate_bounciness` You must namespace the
  technical name with "modname:achievement_name". The technical name of the
  achievement. It must be unique. To ensure uniqueness, namespace the
  achievement with a prefix of the mod that registers it. Required.

  `achievement_def`: is a table with the following elements:

  ```lua
  {
    title = "Ultimate Bounciness",
    -- The title of the achievement, as displayed to the player. Try to make it creative
    description = "Bounce 50 times"
    -- What you need to do to unlock the achievement
    mod_icon = "mymod_icon.png", 
    -- The icon of the mod registering the achievement. If nothing is
    -- provided then a stock trophy image is used. Please provide an icon, 
    -- it helps players understand where the achievement comes from.
    mod_title = "My Mod", -- a pretty, translatable, mod title to display in the tooltip
    -- when the mod icon is hovered. Optional, the mod name will be used instead.
    image = "mymod_ultimate_bounciness.png"
    -- the descriptive image for that achievement. Should be square. If not 16x, then change image_size
    image_size = 16, -- the size of the descriptive image.
    tier = "Gold", -- or "Silver" or "Bronze"
    -- the tier of the achievement. The tier of an achievement is 
    -- indicated in the GUI with an icon named "achievements_lib_<tier_name>.png". 
    -- Gold, Silver, and Bronze tiers are provided by default. To use custom tiers, 
    -- provide icons for them named according to the naming convention.
    hidden = false, -- by default, false. If true then the description will be hidden until unlocked.
    secret = false, -- totally hide achievement (The Title will be "Secret Achievement" and there will be a placeholder image until unlocked)
    on_award = function(p_name) -- function run when awarded. Remember that in some cases the award may occur when the player is offline.
    on_unaward = function(p_name) -- function run when unawarded. Remember that in some cases the removal may occur when the player is offline.

  }

  ```
  After registration, when an achievement definition is returned, it will also
  contain the indexes `mod` and `name` for easy reference.

## Getters

`achvmt_lib.get_achievements(filter)`: returns a table of defined
  achievement defs. If filter is nil, then returns all the achievement defs.

  `filter` (optional) is a table:
  ```lua
  {
    mod = modname, -- modname
    tier = tiername, -- tier name
  }
  ```
  providing a filter table limits the achievements returned to the achievements
  provided by the specified mod or of the specified tier, or both.


`achvmt_lib.get_player_achievements(p_name,return_defs=false,filter)`: 
  returns: `valid_achievements`, `invalid_achievements`

  returns a list of technical names of achievements awarded to the player. Normally,
  with `return_defs` being false or nil, it returns a list of achievement names.
  If the optional `return_defs` is true, it returns a table of achievement defs.

  Player achievements are stored in mod storage as a list of technical names,
  per-player. If a mod that adds achievements is removed, then achievements that a
  player has has been awarded that are not currently defined by a mod will appear
  in `invalid_achievements`. Invalid achievements can only be in the list of names
  format.

  `filter` (optional) is a table:
  ```lua
  {
    mod = modname, -- modname
    tier = tiername, -- tier name
  }
  ```
  providing a filter table limits the achievements returned to the achievements
  provided by the specified mod or of the specified tier, or both.

  Works on offline players and loads their achievements into the
  player_achievements table.

## Awarding and removing achievements

`achvmt_lib.award(achievement_name,p_name)`:
  returns `true` on success, `false` on failure.

  Awards an achievement to player specified by p_name. If the player is online,
  then they receive a HUD popup. If the player is not online, then they will
  receive a hud popup when they next log on.

`achvmt_lib.unaward(achievement_name,p_name)`: 
  returns `true` on success, `false` otherwise.

  Removes an achievement from a player. It is safe to run this on achievements
  that are no longer defined, as gotten from the second result of
  `achvmt_lib.get_player_achievements`.

`achvmt_lib.unaward_all(p_name)`: 
  returns `true` on success, `false` otherwise.

  Removes all achievements from a player.
  `achvmt_lib.get_player_achievements`.

## Checking achievements

`achvmt_lib.has_achievement(p_name,achievement_name)`: returns the index
  of the achievement in `achvmt_lib.player_achievements[p_name]` if the
  player has an achievement already, false otherwise.


## GUI

`achvmt_lib.show_hud_achievement(p_name,achievement_def,lasts_for)`: It is
  the job of this function to show a HUD popup to a player when they are awarded
  an achievement. Override this function to change the default behavior. This
  function should also remove the popup in `lasts_for` seconds.

`achvmt_lib.show_achievements_fs(p_name,to_p_name)` shows the player a
  menu with a scrollable list of achievements. Clicking on one gives more details
  about it. Override this function to implement custom behavior.

  `p_name`: (required) player who has the achievements to show
  `to_p_name`: (optional) player to show the achievements to, defaults to `p_name`

## About the Authors:
Zughy and Freinds is an alliance of free software enthusiasts who make minetest
content. Our minigame server is Arcade Emulation Server, and most of the content
on it is by us. Donate to support our work, especially if this library was
useful to you: https://liberapay.com/Zughy/